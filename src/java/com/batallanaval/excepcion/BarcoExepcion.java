/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.excepcion;

/**
 *
 * @author tec_yohnt
 */
public class BarcoExepcion extends Exception{

    public BarcoExepcion() {
    }

    public BarcoExepcion(String message) {
        super(message);
    }

    public BarcoExepcion(String message, Throwable cause) {
        super(message, cause);
    }

    public BarcoExepcion(Throwable cause) {
        super(cause);
    }

   
    
}
