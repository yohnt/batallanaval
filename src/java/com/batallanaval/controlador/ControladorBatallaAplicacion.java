/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.controlador;

import com.batalla.utilidades.JsfUtil;
import com.batallanaval.excepcion.BarcoExepcion;
import com.batallanaval.modelo.ArbolN;
import com.batallanaval.modelo.BarcoPocicionado;
import com.batallanaval.modelo.Coordenada;
import com.batallanaval.modelo.Estado;
import com.batallanaval.modelo.NodoABB;
import com.batallanaval.modelo.NodoN;
import com.batallanaval.modelo.TipoDeBarco;
import com.batallanaval.utilidades.FacesUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author tec_yohnt
 */
@Named(value = "controladorBatallaAplicacion")
@ApplicationScoped
public class ControladorBatallaAplicacion implements Serializable {

    private ArbolN arbolNuno = new ArbolN();
    private ArbolN arbolNdos = new ArbolN();
    private DefaultDiagramModel model;
    private List<Coordenada> disparosJug1 = new ArrayList<>();
    private List<Coordenada> disparosJug2 = new ArrayList<>();
    private int tamanioTableros;
    private int identificador;
    private int fila;
    private int columna;
    private String verticalUhorizontal;
    private int identificadorD;
    private int filaD;
    private int columnaD;
    private String verticalUhorizontalD;
    private int turno;
    private Boolean disparos;
    private Boolean ganadorUno=false;
    private Boolean ganadorDos= false;
    private Boolean panelPosicionandoUno=true;
    private Boolean panelPosicionandoDos=true;
    private Boolean mostrarPanelDisparosUno= false;
    private Boolean mostrarPanelDisparosDos= false;
   
    
    
   
    
    
    private ControladorBatalla controABB = (ControladorBatalla) FacesUtils.
            getManagedBean("controladorBatalla");

    /**
     * Creates a new instance of ControladorBatallaAplicacion
     */
    public ControladorBatallaAplicacion() {
    }

    public Boolean getPanelPosicionandoUno() {
        return panelPosicionandoUno;
    }

    public void setPanelPosicionandoUno(Boolean panelPosicionandoUno) {
        this.panelPosicionandoUno = panelPosicionandoUno;
    }

    public Boolean getPanelPosicionandoDos() {
        return panelPosicionandoDos;
    }

    public void setPanelPosicionandoDos(Boolean panelPosicionandoDos) {
        this.panelPosicionandoDos = panelPosicionandoDos;
    }

    public Boolean getMostrarPanelDisparosUno() {
        return mostrarPanelDisparosUno;
    }

    public void setMostrarPanelDisparosUno(Boolean mostrarPanelDisparosUno) {
        this.mostrarPanelDisparosUno = mostrarPanelDisparosUno;
    }

    public Boolean getMostrarPanelDisparosDos() {
        return mostrarPanelDisparosDos;
    }

    public void setMostrarPanelDisparosDos(Boolean mostrarPanelDisparosDos) {
        this.mostrarPanelDisparosDos = mostrarPanelDisparosDos;
    }
    
    public Boolean getGanadorUno() {
        return ganadorUno;
    }

    public void setGanadorUno(Boolean ganadorUno) {
        this.ganadorUno = ganadorUno;
    }

    public Boolean getGanadorDos() {
        return ganadorDos;
    }

    public void setGanadorDos(Boolean ganadorDos) {
        this.ganadorDos = ganadorDos;
    }
    
    
    public Boolean getDisparos() {
        return disparos;
    }

    public void setDisparos(Boolean disparos) {
        this.disparos = disparos;
    }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }
    
    public List<Coordenada> getDisparosJug2() {
        return disparosJug2;
    }

    public void setDisparosJug2(List<Coordenada> disparosJug2) {
        this.disparosJug2 = disparosJug2;
    }

    public int getIdentificadorD() {
        return identificadorD;
    }

    public void setIdentificadorD(int identificadorD) {
        this.identificadorD = identificadorD;
    }

    public int getFilaD() {
        return filaD;
    }

    public void setFilaD(int filaD) {
        this.filaD = filaD;
    }

    public int getColumnaD() {
        return columnaD;
    }

    public void setColumnaD(int columnaD) {
        this.columnaD = columnaD;
    }

    public String getVerticalUhorizontalD() {
        return verticalUhorizontalD;
    }

    public void setVerticalUhorizontalD(String verticalUhorizontalD) {
        this.verticalUhorizontalD = verticalUhorizontalD;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public String getVerticalUhorizontal() {
        return verticalUhorizontal;
    }

    public void setVerticalUhorizontal(String verticalUhorizontal) {
        this.verticalUhorizontal = verticalUhorizontal;
    }

   

    
   
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }
   
    public int getTamanioTableros() {
        return tamanioTableros;
    }

    public void setTamanioTableros(int tamanioTableros) {
        this.tamanioTableros = tamanioTableros;
    }

    public List<Coordenada> getDisparosJug1() {
        return disparosJug1;
    }

    public void setDisparosJug1(List<Coordenada> disparosJug1) {
        this.disparosJug1 = disparosJug1;
    }
    
    public ArbolN getArbolNdos() {
        return arbolNdos;
    }

    public void setArbolNdos(ArbolN arbolNdos) {
        this.arbolNdos = arbolNdos;
    }
    
    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public ControladorBatalla getControABB() {
        return controABB;
    }

    public void setControABB(ControladorBatalla controABB) {
        this.controABB = controABB;
    }

    public ArbolN getArbolNuno() {
        return arbolNuno;
    }

    public void setArbolNuno(ArbolN arbolNuno) {
        this.arbolNuno = arbolNuno;
    }

 //este lo debo quitar    
//     @PostConstruct
    public void cargarJugadores() {
        cargarArbol();
        cargarArbolDos();
        controABB.setMostrarArbolBarcosJugar(false);
        pintarArbol();
        controABB.setMostrarPanelBarcosCargados(true);
        calcularTamanioTableros();

    }

    public void cargarArbol() {

        try {
            //Algoritmo
            arbolNuno.adicionaNodo(new BarcoPocicionado(new TipoDeBarco("YO", (byte) 0, (byte) 0), (byte) 0, new Estado((byte) 0, "")), null);

            List<BarcoPocicionado> padres = new ArrayList<BarcoPocicionado>();
            padres.add(arbolNuno.getRaiz().getDato());
            adicionarPreOrden(controABB.getArbol().getRaiz(), padres, 0);

            System.out.println("Cantidad: " + controABB.getArbol().
                    getCantidadNodos());

        } catch (BarcoExepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }

    }

    private void adicionarPreOrden(NodoABB reco, List<BarcoPocicionado> padres, int contizq) throws BarcoExepcion {
        if (reco != null) {
            List<BarcoPocicionado> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidadJuego(); i++) {
                BarcoPocicionado barcoNuevo = new BarcoPocicionado(new TipoDeBarco(reco.getDato().getNombre(), reco.getDato().getNumeroCasillas(),
                        reco.getDato().getCantidadJuego()), (byte) 0, new Estado((byte) 0, ""));

                barcoNuevo.setIdentificador(++contizq);
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolNuno.adicionarNodoxCodigo(barcoNuevo, padres.get(contPapas));
                padresNuevos.add(barcoNuevo);
                contPapas++;
            }
            adicionarPreOrden(reco.getIzquierda(), padresNuevos, contizq);
            contizq = contizq + controABB.getArbol().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrden(reco.getDerecha(), padresNuevos, contizq);

        }
    }
    
    
    
    
    public void cargarArbolDos() {

        try {
            //Algoritmo
            arbolNdos.adicionaNodo(new BarcoPocicionado(new TipoDeBarco("YO", (byte) 0, (byte) 0), (byte) 0, new Estado((byte) 0, "")), null);

            List<BarcoPocicionado> padres = new ArrayList<BarcoPocicionado>();
            padres.add(arbolNdos.getRaiz().getDato());
            adicionarPreOrdenDos(controABB.getArbol().getRaiz(), padres, 0);
            
            System.out.println("Cantidad: " + controABB.getArbol().
                    getCantidadNodos());

        } catch (BarcoExepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }

    }

    private void adicionarPreOrdenDos(NodoABB reco, List<BarcoPocicionado> padres, int contizq) throws BarcoExepcion {
        if (reco != null) {
            List<BarcoPocicionado> padresNuevos = new ArrayList<>();
            int contPapas = 0;
            for (byte i = 0; i < reco.getDato().getCantidadJuego(); i++) {
                BarcoPocicionado barcoNuevo = new BarcoPocicionado(new TipoDeBarco(reco.getDato().getNombre(), reco.getDato().getNumeroCasillas(),
                        reco.getDato().getCantidadJuego()), (byte) 0, new Estado((byte) 0, ""));

                barcoNuevo.setIdentificador(++contizq);
                if (contPapas >= padres.size()) {
                    contPapas = 0;
                }
                arbolNdos.adicionarNodoxCodigo(barcoNuevo, padres.get(contPapas));
                padresNuevos.add(barcoNuevo);
                contPapas++;
            }
            adicionarPreOrdenDos(reco.getIzquierda(), padresNuevos, contizq);
            contizq = contizq + controABB.getArbol().sumarInOrdenRecursivo(reco.getIzquierda());
            adicionarPreOrdenDos(reco.getDerecha(), padresNuevos, contizq);

        }
    }
    
    
    
    

    public void pintarArbol() {

        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbolNuno.getRaiz(), model, null, 30, 0);

    }

    private void pintarArbol(NodoN reco, DefaultDiagramModel model, Element padre, int x, int y) {

        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());

            elementHijo.setX(String.valueOf(x) + "em");
            elementHijo.setY(String.valueOf(y) + "em");
            //elementHijo.setId(reco.getDato().getNroIdentificacion());

            if (padre != null) {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre = new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));

            }

            model.addElement(elementHijo);
            for (NodoN hijo : reco.getHijos()) {
                pintarArbol(hijo, model, elementHijo, x - 10, y + 5);
                x += 10;
            }
        }
    }

    public void disparar(int fila, int columna)
    {
        disparosJug1.add(new Coordenada((byte)fila, (byte)columna));
        
         disparos =  arbolNdos.disparar(fila, columna);
         asignarTurnos();
        verificarDisparo(fila, columna);
        
     
        
        arbolNdos.finalizarNodos();
      
        JsfUtil.addSuccessMessage("Disparó en ["+fila+","+columna+"]");
       
        if ( arbolNdos.verificarGanador()==true){
            ganadorUno=true;
            mostrarPanelDisparosUno=false;
        }
         
    }        
    
    
    public boolean verificarDisparo(int fila, int columna)
    {
        
        if(turno%2!=0){
            return true;
        }
        
        for(Coordenada disp: disparosJug1)
        {
            if(disp.getFila()==fila  && disp.getColumna()==columna)
            {
                return true;
            }
        }
       
        return false;
    }
    
    
      public void dispararJugadorDos(int fila, int columna)
    {
        disparosJug2.add(new Coordenada((byte)fila, (byte)columna));
         disparos=  arbolNuno.disparar(fila, columna);
         asignarTurnos();
        verificarDisparosDos(fila, columna);
//        arbolNuno.disparar(fila, columna);
    
         
       
        
        arbolNuno.finalizarNodos();
        
        JsfUtil.addSuccessMessage("Disparó en ["+fila+","+columna+"]");
       
        if (arbolNuno.verificarGanador()==true){
            ganadorDos=true;
            mostrarPanelDisparosDos=false;
        }
       
        
    }        
    
    
    public boolean verificarDisparosDos(int fila, int columna)
    {
       
        if (turno%2==0){
            return true;
        }
        
        for(Coordenada disp: disparosJug2)
        {
            if(disp.getFila()==fila  && disp.getColumna()==columna)
            {
                return true;
            }
        }
        return false;
    }
//     public String pintarCoordenada(int fila, int columna)
//    {
//        for(Barco barquito: misBarquitos)
//        {
//            if(barquito.verificarCoordenada(fila, columna))
//            {
//                if(barquito.getNombre().compareTo("chalupa")==0)
//                {    
//                return "width: 80px; height: 80px; background-color: green;";
//                }
//                else
//                {
//                    return "width: 80px; height: 80px; background-color: yellow;";
//                }
//            }
//        }
//        return "width: 80px; height: 80px; background-color: blue;";
//    }
    public void calcularTamanioTableros(){
        if (arbolNuno.getCantidadNodos()>1&& arbolNuno.getCantidadNodos()<=10){
            tamanioTableros=10;
        }
        else if (arbolNuno.getCantidadNodos()<=20){
            tamanioTableros=20;
        }
    }
    public void posicionarBarcosPrimerJugador(){
//        if (identificador < arbolNuno.getCantidadNodos()){
            
       
      if(arbolNuno.buscarNodoN(identificador).getDato().getCoordenadas().size()==0){ 
          
          
          
          
     arbolNuno.adicionarCoordenadas(identificador, fila, columna, verticalUhorizontal);
     habilitarPanelDisparosUno();
      }
      else {
          JsfUtil.addErrorMessage("El barco ya esta pocicionado");
      }
      
//       }
//        else {
//            JsfUtil.addErrorMessage("No se puede posicionar este barco");
//        }
        
    }
    
    public void posicionarBarcoSegundoJugador(){
//        if (identificador < arbolNuno.getCantidadNodos()){
            
       
        
       if (arbolNdos.buscarNodoN(identificadorD).getDato().getCoordenadas().size()==0){
           arbolNdos.adicionarCoordenadas(identificadorD, filaD, columnaD, verticalUhorizontalD);
           habilitarPanelDisparosDos();
       } 
       else {
           JsfUtil.addErrorMessage("El barco ya esta pocicionado");
       }
//        }
//        else {
//            JsfUtil.addErrorMessage("No se puede posicionar el barco");
//        }
    }
    
    
    
    
    public String pintarCoordenadas(int fila, int columna){
       
        for (int i=0;i<arbolNuno.getCantidadNodos();i++){
            
          if(arbolNuno.verificarCoordenada(fila, columna, i)){
              
              if(arbolNuno.buscarNodoN(identificador).getDato().getTipoDeBarco().getNombre().equals("Perla")){
                   return "width: 70px; height: 70px; background-color: green ;"; 
              }
              else {
                   return "width: 70px; height: 70px; background-color: brown ;"; 
              }   
          }
        }
        return "width: 70px; height: 70px; background-color: blue ;"; 
        } 
     public String pintarCoordenadasJugadorDos(int fila, int columna){
       
        for (int i=0;i<arbolNdos.getCantidadNodos();i++){
            
          if(arbolNdos.verificarCoordenadaJugadorDos(fila, columna, i)){
              
              if(arbolNdos.buscarNodoN(identificador).getDato().getTipoDeBarco().getNombre().equals("Perla")){
                   return "width: 70px; height: 70px; background-color: green ;"; 
              }
              else {
                   return "width: 70px; height: 70px; background-color: brown ;"; 
              }   
          }
        }
        return "width: 70px; height: 70px; background-color: blue;"; 
        } 
     public void asignarTurnos(){
         if (disparos==true){
             turno=turno;
         }
         else {
             turno++;
         }
     }
     public void iniciarTurno(){
         int numero= (int) (Math.random() * 10 + 1);
         turno=numero;
         if (turno%2==0){
             JsfUtil.addSuccessMessage("Inicia el jugador UNO");
         }
         else {
             JsfUtil.addSuccessMessage("Inicia el jugador DOS");
         }
     }
     public void habilitarPanelDisparosUno(){
         int cont=0;
         for(int i=0; i< arbolNuno.getCantidadNodos();i++){
             if(arbolNuno.buscarNodoN(i).getDato().getEstado().getDescripcion().equals("1")){
                 cont ++;
             }
         }
         if (arbolNuno.getCantidadNodos()-1==cont){
             panelPosicionandoUno=false;
             mostrarPanelDisparosUno= true;
         }
     }
     
     public void habilitarPanelDisparosDos(){
         int cont=0;
         for(int i=0; i< arbolNdos.getCantidadNodos();i++){
             if(arbolNdos.buscarNodoN(i).getDato().getEstado().getDescripcion().equals("1")){
                 cont ++;
             }
         }
         if (arbolNdos.getCantidadNodos()-1==cont){
             panelPosicionandoDos=false;
             mostrarPanelDisparosDos= true;
         }
     }
     
}
