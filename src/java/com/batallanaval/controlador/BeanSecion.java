/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.controlador;

import com.arbolesprog3.utilidades.JsfUtil;
import com.batallanaval.modelo.Rol;
import com.batallanaval.modelo.Usuario;
import com.batallanaval.utilidades.FacesUtils;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author tec_yohnt
 */
@Named(value = "beanSecion")
@ApplicationScoped
public class BeanSecion implements Serializable {
    
   private ControladorBatalla control;
   private String usuarioIngresado;
   private String contraseñaIngresada;
   private List<Usuario> usuarios=new ArrayList<>();
   private Boolean habilitarPanel=true;
   private Boolean mostrarSegundoPanel=false;
    private String contrasenaIngresada;
    private String usuariosIngresado;
     private ControladorBatalla controABB=(ControladorBatalla) FacesUtils.
                     getManagedBean("controladorBatalla");
    
    /**
     * Creates a new instance of BeanSecion
     */
    public BeanSecion() {
    }

    public ControladorBatalla getControABB() {
        return controABB;
    }

    public void setControABB(ControladorBatalla controABB) {
        this.controABB = controABB;
    }

    public String getContrasenaIngresada() {
        return contrasenaIngresada;
    }

    public void setContrasenaIngresada(String contrasenaIngresada) {
        this.contrasenaIngresada = contrasenaIngresada;
    }

    public String getUsuariosIngresado() {
        return usuariosIngresado;
    }

    public void setUsuariosIngresado(String usuariosIngresado) {
        this.usuariosIngresado = usuariosIngresado;
    }

    public Boolean getMostrarSegundoPanel() {
        return mostrarSegundoPanel;
    }

    public void setMostrarSegundoPanel(Boolean mostrarSegundoPanel) {
        this.mostrarSegundoPanel = mostrarSegundoPanel;
    }

    public ControladorBatalla getControl() {
        return control;
    }

    public void setControl(ControladorBatalla control) {
        this.control = control;
    }

    public Boolean getHabilitarPanel() {
        return habilitarPanel;
    }

    public void setHabilitarPanel(Boolean habilitarPanel) {
        this.habilitarPanel = habilitarPanel;
    }
  
    public BeanSecion(ControladorBatalla control) {
        this.control = control;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
    

    public String getUsuarioIngresado() {
        return usuarioIngresado;
    }

    public void setUsuarioIngresado(String usuarioIngresado) {
        this.usuarioIngresado = usuarioIngresado;
    }

    public String getContraseñaIngresada() {
        return contraseñaIngresada;
    }

    public void setContraseñaIngresada(String contraseñaIngresada) {
        this.contraseñaIngresada = contraseñaIngresada;
    }
    @PostConstruct
    public void inicio(){
        llenarUsuariosBean();
    }
    public void llenarUsuariosBean(){
        usuarios.add(new Usuario("samu", "1", new Rol(  (byte)1, "Administrador")));
    }
    
    public String autenticacion(){
  
        for (int i=0; i<usuarios.size();i++){
          if (usuarios.get(i).getContrasena().equals(contraseñaIngresada)&&
                  usuarios.get(i).getCorreo().equals(usuarioIngresado)){
              habilitarPanel= false;
              mostrarSegundoPanel= true;
              return "irNombreDeBarcos";
          } 
            JsfUtil.addErrorMessage("Usuario o contraseña incorrecta");
        }
        return "";
    }
    
    
    public String autenticacionJugadores(){
  
        for (int i=0; i<controABB.getListadoUsuarios().size();i++){
            
          if (controABB.getListadoUsuarios().get(i).getContrasena().equals(contrasenaIngresada)&&
                  controABB.getListadoUsuarios().get(i).getCorreo().equals(usuariosIngresado) &&
                  controABB.getListadoUsuarios().get(i).getTipoRol().getNombre().equals("1"))  {
              return "irJugadorUno";
          } 
          if (controABB.getListadoUsuarios().get(i).getContrasena().equals(contrasenaIngresada)&& 
                  controABB.getListadoUsuarios().get(i).getCorreo().equals(usuariosIngresado)&&
                  controABB.getListadoUsuarios().get(i).getTipoRol().getNombre().equals("2")){
              return "irJugadorDos";
          }
          
        }
          JsfUtil.addErrorMessage("Usuario o contraseña incorrecta");
        return "";
    }
}
