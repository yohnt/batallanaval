/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.controlador;

import com.arbolesprog3.utilidades.JsfUtil;
import com.batallanaval.excepcion.BarcoExepcion;
import com.batallanaval.modelo.ArbolABB;
import com.batallanaval.modelo.NodoABB;
import com.batallanaval.modelo.NombreDeBarcos;
import com.batallanaval.modelo.Rol;
import com.batallanaval.modelo.TipoDeBarco;
import com.batallanaval.modelo.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

/**
 *
 * @author tec_yohnt
 */
@Named(value = "controladorBatalla")
@ApplicationScoped

public class ControladorBatalla implements Serializable {

    private List<Usuario> listadoUsuarios = new ArrayList<>();
    private List<NombreDeBarcos> listadoNombres = new ArrayList<>();
    private boolean mostrarTablaNombres = true;
    private Boolean mostrarPaneBarcos = true;
    private String nombreUltimo;
    private Boolean botonAtras= false;
    private Boolean panelUsuarios= false;
    private String nombre = "";
    private ArbolABB arbol= new ArbolABB();
    private Rol roles;
    private byte numeroUno;
    private byte numeroDos;
    private String contrasena;
    private String correo;
    private String tipoJugador; 
    private Boolean limiteUsuarios= false;
    private Boolean mostrarListadoUsuarios=false;
    private TipoDeBarco barco= new TipoDeBarco();
    private DefaultDiagramModel model;
    private Boolean mostrarArbolBarcosJugar= false;
    private Boolean mostrarPanelBarcosCargados= false;
   
    
    public TipoDeBarco getBarco() {
        return barco;
    }

    
    

    public void setBarco(TipoDeBarco barco) {
        this.barco = barco;
    }

    public Boolean getMostrarArbolBarcosJugar() {
        return mostrarArbolBarcosJugar;
    }

    public void setMostrarArbolBarcosJugar(Boolean mostrarArbolBarcosJugar) {
        this.mostrarArbolBarcosJugar = mostrarArbolBarcosJugar;
    }

    public Boolean getMostrarPanelBarcosCargados() {
        return mostrarPanelBarcosCargados;
    }

    public void setMostrarPanelBarcosCargados(Boolean mostrarPanelBarcosCargados) {
        this.mostrarPanelBarcosCargados = mostrarPanelBarcosCargados;
    }
    
    

    
    
    
    public Boolean getMostrarListadoUsuarios() {
        return mostrarListadoUsuarios;
    }

    public void setMostrarListadoUsuarios(Boolean mostrarListadoUsuarios) {
        this.mostrarListadoUsuarios = mostrarListadoUsuarios;
    }

    
    
    
    public Boolean getLimiteUsuarios() {
        return limiteUsuarios;
    }

    public void setLimiteUsuarios(Boolean limiteUsuarios) {
        this.limiteUsuarios = limiteUsuarios;
    }
    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTipoJugador() {
        return tipoJugador;
    }

    public void setTipoJugador(String tipoJugador) {
        this.tipoJugador = tipoJugador;
    }
            
            
            
    public Boolean getPanelUsuarios() {
        return panelUsuarios;
    }

    public void setPanelUsuarios(Boolean panelUsuarios) {
        this.panelUsuarios = panelUsuarios;
    }

    public Boolean getBotonAtras() {
        return botonAtras;
    }

    public void setBotonAtras(Boolean botonAtras) {
        this.botonAtras = botonAtras;
    }

    public Boolean getMostrarPaneBarcos() {
        return mostrarPaneBarcos;
    }

    public void setMostrarPaneBarcos(Boolean mostrarPaneBarcos) {
        this.mostrarPaneBarcos = mostrarPaneBarcos;
    }

    public byte getNumeroDos() {
        return numeroDos;
    }

    public void setNumeroDos(byte numeroDos) {
        this.numeroDos = numeroDos;
    }
    
    

    public byte getNumeroUno() {
        return numeroUno;
    }

    public void setNumeroUno(byte numeroUno) {
        this.numeroUno = numeroUno;
    }

   
    

    public String getNombreUltimo() {
        return nombreUltimo;
    }

    public void setNombreUltimo(String nombreUltimo) {
        this.nombreUltimo = nombreUltimo;
    }

    public boolean isMostrarTablaNombres() {
        return mostrarTablaNombres;
    }

    public void setMostrarTablaNombres(boolean mostrarTablaNombres) {
        this.mostrarTablaNombres = mostrarTablaNombres;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<NombreDeBarcos> getListadoNombres() {
        return listadoNombres;
    }

    public void setListadoNombres(List<NombreDeBarcos> listadoNombres) {
        this.listadoNombres = listadoNombres;
    }

    public ArbolABB getArbol() {
        return arbol;
    }

    public void setArbol(ArbolABB arbol) {
        this.arbol = arbol;
    }

    public List<Usuario> getListadoUsuarios() {
        return listadoUsuarios;
    }

    public void setListadoUsuarios(List<Usuario> listadoUsuarios) {
        this.listadoUsuarios = listadoUsuarios;
    }

    public Rol getRoles() {
        return roles;
    }

    public void setRoles(Rol roles) {
        this.roles = roles;
    }

    @PostConstruct
    public void inicio() {
        llenarUsuarios();
        llenarNombres();
         llenarBarcos();
         pintarArbol();

    }

    public void llenarUsuarios() {

        listadoUsuarios.add(new Usuario("samu", "1", new Rol( (byte)1, "Administrador")));
        
    }
    
    
    

    public void llenarNombres()  {
        listadoNombres.add(new NombreDeBarcos("Holandes"));
        listadoNombres.add(new NombreDeBarcos("Titanic"));
        listadoNombres.add(new NombreDeBarcos("Perla"));
        listadoNombres.add(new NombreDeBarcos("Nautilus"));
        listadoNombres.add(new NombreDeBarcos("Victoria"));
        listadoNombres.add(new NombreDeBarcos("SanM"));
        listadoNombres.add(new NombreDeBarcos("Ranger"));

    }
    
    
    
    
    public void llenarBarcos(){
        
        
        try {
          arbol.adicionarNodo(new TipoDeBarco("Perla",(byte) 3,(byte)1 ));
        arbol.adicionarNodo(new TipoDeBarco("Victoria",(byte) 4,(byte)1 ));
//        arbol.adicionarNodo(new TipoDeBarco("SamM",(byte) 2,(byte)1 ));
//        arbol.adicionarNodo(new TipoDeBarco("Ranger",(byte) 4,(byte)2));
//        arbol.adicionarNodo(new TipoDeBarco("Titanic",(byte) 9,(byte)2 ));  
        } catch (BarcoExepcion e) {
        }
        
    }

    public void ingresarNombresBarcos() {

        listadoNombres.add(new NombreDeBarcos(nombre));

    }

    public void deshabilitarTablaNombres() {
        mostrarTablaNombres = false;
    }

    public void habilitarTablaNombres() {
        mostrarTablaNombres = true;
        mostrarArbolBarcosJugar=false;
        mostrarPanelBarcosCargados= false;
    }
public void inhabilitarPanelBarcos(){
    mostrarPaneBarcos=false;
    botonAtras=true;
    panelUsuarios= true;
    mostrarListadoUsuarios=true;
    
}
public void habilitarPanelBarcos(){
    mostrarPaneBarcos=true;
    botonAtras = false;
    panelUsuarios= false;
    mostrarListadoUsuarios=false;
}
    public List<NombreDeBarcos> listaPintar() {
        List<NombreDeBarcos> listado = new ArrayList<>();
        for (int i = 0; i < listadoNombres.size(); i++) {
            listado.add(listadoNombres.get(i));
        }
        return listado;
    }
    public void crearUsuarios(){
        listadoUsuarios.add(new Usuario(correo, contrasena,new Rol((byte)1, tipoJugador)));
        habilitarLimiteUsuarios();
        
        
        
        
        
        
    }
    //Metodo para listar usuarios 
    
    public List<Usuario> listarUsuarios(){
        List<Usuario> listado= new ArrayList<>();
        for(int i=0;i<listadoUsuarios.size();i++){
            listado.add(listadoUsuarios.get(i));
        }
        return listado;
    }
    public void habilitarLimiteUsuarios(){
        if (listadoUsuarios.size()>2){
            limiteUsuarios=true;
        }
    }
    public void guardarBarco() {

        try {
            arbol.adicionarNodo(barco);
            barco = new TipoDeBarco();
            
        } catch (BarcoExepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        
        pintarArbol();
        habilitarMotrarBarcosJugar();
        
        
        
    }
    public void pintarArbol()
    {
        
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.setConnectionsDetachable(false);
        StraightConnector connector = new StraightConnector();
        connector.setPaintStyle("{strokeStyle:'#404a4e', lineWidth:2}");
        connector.setHoverPaintStyle("{strokeStyle:'#20282b'}");
        model.setDefaultConnector(connector);
        pintarArbol(arbol.getRaiz(), model, null, 30, 0);
        
    }
    
    
    

    private void pintarArbol(NodoABB reco,DefaultDiagramModel model, Element padre, int x, int y) {
               
        if (reco != null) {
            Element elementHijo = new Element(reco.getDato());
            
            elementHijo.setX(String.valueOf(x)+"em");
            elementHijo.setY(String.valueOf(y)+"em");
            elementHijo.setId(String.valueOf(reco.getDato().getNumeroCasillas()));
            
            if(padre!=null)
            {
                elementHijo.addEndPoint(new DotEndPoint(EndPointAnchor.TOP));
                DotEndPoint conectorPadre=new DotEndPoint(EndPointAnchor.BOTTOM);
                padre.addEndPoint(conectorPadre);                
                model.connect(new Connection(conectorPadre, elementHijo.getEndPoints().get(0)));        
                
            }    
            
            model.addElement(elementHijo);
            
            pintarArbol(reco.getIzquierda(),model, elementHijo,x-5,y+5);
            pintarArbol(reco.getDerecha(),model,elementHijo,x+5, y+5);
        }
    }
     private EndPoint createEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setStyle("{fillStyle:'#404a4e'}");
        endPoint.setHoverStyle("{fillStyle:'#20282b'}");
         
        return endPoint;
    }
     
    public DiagramModel getModel() {
        return model;
    }
    
    
    
    
    public void habilitarMotrarBarcosJugar(){
      mostrarArbolBarcosJugar= true;  
    }
    
}

