/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.validador;

import com.batallanaval.excepcion.BarcoExepcion;
import com.batallanaval.modelo.TipoDeBarco;



/**
 *
 * @author carloaiza
 */
public class BarcoValidador {

     public static void validarDatos(TipoDeBarco barco) throws BarcoExepcion {
        if (barco.getNombre() == null || barco.getNombre().equals("")
                || barco.getNombre().startsWith(" ")) {
            throw new BarcoExepcion("Debe diligenciar nombre");
        }
        if (barco.getNumeroCasillas() == 0 )
                {
            throw new BarcoExepcion("Debe diligenciar numero de casillas");
        }
        if (barco.getCantidadJuego() == 0 )
                {
            throw new BarcoExepcion("Debe diligenciar la cantidad en el juego");
        }
    }
}
