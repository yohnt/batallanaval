/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.modelo;

import com.batalla.utilidades.JsfUtil;
import com.batallanaval.excepcion.BarcoExepcion;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tec_yohnt
 */
public class ArbolN {

    private NodoN raiz;
    private int cantidadNodos;
    private List<Coordenada> coordenadaOcupada = new ArrayList<>();
    private NodoN retorno;

    public NodoN getRetorno() {
        return retorno;
    }

    public List<Coordenada> getCoordenadaOcupada() {
        return coordenadaOcupada;
    }

    public void setCoordenadaOcupada(List<Coordenada> coordenadaOcupada) {
        this.coordenadaOcupada = coordenadaOcupada;
    }

    public void setRetorno(NodoN retorno) {
        this.retorno = retorno;
    }

    public NodoN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoN raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public void adicionaNodo(BarcoPocicionado dato, BarcoPocicionado padre) throws BarcoExepcion {
        if (raiz == null) {
            raiz = new NodoN(dato);

        } else {
            adicionarNodo(dato, padre, raiz);

        }
        cantidadNodos++;

    }

    public boolean adicionarNodo(BarcoPocicionado dato, BarcoPocicionado padre, NodoN pivote) throws BarcoExepcion {
        // boolean adicionado=false;

        String padr = String.valueOf(padre.getTipoDeBarco().getCantidadJuego());
        String dat = String.valueOf(dato.getTipoDeBarco().getNumeroCasillas());
        if (dat.compareTo(padr) == 0) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoN(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                if (adicionarNodo(dato, padre, hijo)) {
                    break;
                }
            }
        }
        return false;
    }

    public void adicionarNodoxCodigo(BarcoPocicionado dato, BarcoPocicionado padre) throws BarcoExepcion {
        if (raiz == null) {
            raiz = new NodoN(dato);

        } else {
            adicionarNodoxCodigo(dato, padre, raiz);

        }
        cantidadNodos++;

    }

    private boolean adicionarNodoxCodigo(BarcoPocicionado dato, BarcoPocicionado padre, NodoN pivote) throws BarcoExepcion {
        // boolean adicionado=false;
        if (pivote.getDato().getIdentificador() == padre.getIdentificador()) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoN(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                if (adicionarNodoxCodigo(dato, padre, hijo)) {
                    break;
                }

            }
        }
        return false;
    }

    public NodoN buscarNodoN(int identificador) {

        return buscarNodoN(identificador, raiz);

    }

    private NodoN buscarNodoN(int identificador, NodoN pivote) {
       
        
        if (pivote.getDato().getIdentificador() == identificador) {
            return pivote;
        } else {
            NodoN encontrado = null;
            for (NodoN hijo : pivote.getHijos()) {
                encontrado = buscarNodoN(identificador, hijo);
                if (encontrado != null) {
                    break;
                }
            }

            return encontrado;

        }
        

        
        
        
        
    }

    public void adicionarCoordenadas(int identificador, int fila, int columna, String verticalUhorizontal) {
        
        if(identificador<=0|| identificador>cantidadNodos-1){
            JsfUtil.addErrorMessage("No se puede adicionar barco");
            
        }
        else{

        if (verificarCoordenadaOcupada(fila, columna, verticalUhorizontal, identificador) == true) {

            NodoN nodoEcontrado = buscarNodoN(identificador);

            for (int i = 0; i < nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas(); i++) {
                if (verticalUhorizontal.equals("Horizontal")) {
                    int colum = columna + i;
                    if (cantidadNodos <= 10) {
                        if (nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas() + columna <= 11) {
                            nodoEcontrado.getDato().getCoordenadas().add(new Coordenada((byte) fila, (byte) colum));
                            nodoEcontrado.getDato().getEstado().setDescripcion("1");
                            
                            coordenadaOcupada.add(new Coordenada((byte) fila, (byte) colum));

                        } else {
                            JsfUtil.addErrorMessage("Tamaño del barco es " + nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas()
                                    + " debe seleccionar otra columna");
                            break;
                        }
                    } else {
                        if (nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas() + columna <= 21) {
                            nodoEcontrado.getDato().getCoordenadas().add(new Coordenada((byte) fila, (byte) colum));
                            nodoEcontrado.getDato().getEstado().setDescripcion("1");
                            coordenadaOcupada.add(new Coordenada((byte) fila, (byte) colum));
                        } else {
                            JsfUtil.addErrorMessage("Tamaño del barco es " + nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas()
                                    + " debe seleccionar otra columna");
                            break;
                        }
                    }
                } //Aqui se cambia para llenar coordenadas vertical
                else {
                    int fil = fila + i;

                    if (cantidadNodos <= 10) {
                        if (nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas() + fila <= 11) {
                            nodoEcontrado.getDato().getCoordenadas().add(new Coordenada((byte) fil, (byte) columna));
                            nodoEcontrado.getDato().getEstado().setDescripcion("1");
                            coordenadaOcupada.add(new Coordenada((byte) fil, (byte) columna));
                        } else {
                            JsfUtil.addErrorMessage("Tamaño del barco es " + nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas()
                                    + " debe seleccionar otra fila");
                            break;
                        }
                    } else {
                        if (nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas() + fila <= 21) {
                            nodoEcontrado.getDato().getCoordenadas().add(new Coordenada((byte) fil, (byte) columna));
                            nodoEcontrado.getDato().getEstado().setDescripcion("1");
                            coordenadaOcupada.add(new Coordenada((byte) fil, (byte) columna));
                        } else {
                            JsfUtil.addErrorMessage("Tamaño del barco es " + nodoEcontrado.getDato().getTipoDeBarco().getNumeroCasillas()
                                    + " debe seleccionar otra fila");
                            break;
                        }
                    }
                }
            }
        } else {
            JsfUtil.addErrorMessage("Hay un barco ocupango alguna coordenada de las seleccionada");
        }
        }
    }

    public boolean verificarCoordenada(int fila, int columna, int identifica) {
        for (Coordenada coord : buscarNodoN(identifica).getDato().getCoordenadas()) {
            if (coord.getFila() == fila && coord.getColumna() == columna) {
                return true;
            }
        }
        return false;
    }

    public boolean verificarCoordenadaJugadorDos(int fila, int columna, int identifica) {
        for (Coordenada coord : buscarNodoN(identifica).getDato().getCoordenadas()) {
            if (coord.getFila() == fila && coord.getColumna() == columna) {
                return true;
            }
        }
        return false;
    }

    public Boolean verificarCoordenadaOcupada(int fila, int columna, String horizontalVertical, int identificador) {
        NodoN nodoEncontrado = buscarNodoN(identificador);
        int columnaSuplen = columna - 1;
        int filaSuplen = fila - 1;
        int horizo = 0;
        int verti = 0;

        //Horizontal
        if (horizontalVertical.equals("Horizontal")) {

            for (int i = columna; i < nodoEncontrado.getDato().getTipoDeBarco().getNumeroCasillas() + columna; i++) {

                horizo = columnaSuplen++;
                for (int y = 0; y < coordenadaOcupada.size(); y++) {

                    if (coordenadaOcupada.get(y).getColumna() == horizo && coordenadaOcupada.get(y).getFila() == fila) {
                        return false;
                    }
                }

            }
            return true;

        } else {
            //Vertical
            for (int j = fila; j < nodoEncontrado.getDato().getTipoDeBarco().getNumeroCasillas() + fila; j++) {
                verti = filaSuplen++;
                for (int m = 0; m < coordenadaOcupada.size(); m++) {

                    if (coordenadaOcupada.get(m).getFila() == verti && coordenadaOcupada.get(m).getColumna() == columna) {
                        return false;
                    }
                }
            }

        }
        return true;
    }

    public Boolean disparar(int fila, int columna) {
        for (int i = 0; i < cantidadNodos; i++) {

            NodoN nodoEncontrado = buscarNodoN(i);
            if (nodoEncontrado.getDato().getCoordenadas().size() > 0) {
                for (int y = 0; y < nodoEncontrado.getDato().getCoordenadas().size(); y++) {
                    if (nodoEncontrado.getDato().getCoordenadas().get(y).getFila() == fila
                            && nodoEncontrado.getDato().getCoordenadas().get(y).getColumna() == columna) {
                        byte impacto = nodoEncontrado.getDato().getNumeroImpactos();
                        byte impactoFinal = (byte) (impacto + 1);
                        nodoEncontrado.getDato().setNumeroImpactos((byte) impactoFinal);
                         return true;
                    }
                }
            }

        }
        return false;
    }
    public void finalizarNodos(){
        for (int i=0;i<cantidadNodos;i++){
       NodoN  nodoEncontrado= buscarNodoN(i);
       if(nodoEncontrado.getDato().getNumeroImpactos()>0){
           if (nodoEncontrado.getDato().getTipoDeBarco().getNumeroCasillas()== nodoEncontrado.getDato().getNumeroImpactos()){
               nodoEncontrado.getDato().getEstado().setCodigo((byte) 1);
               JsfUtil.addSuccessMessage("Esta derribado   "+ nodoEncontrado.getDato().getTipoDeBarco().getNombre()+
                       "  Con el ID  "+ nodoEncontrado.getDato().getIdentificador());
               
           }
       }
            
        }
    }
    public Boolean verificarGanador(){
        int resultado=0;
        for(int i=0;i<cantidadNodos;i++){
            NodoN nodoEncontrado= buscarNodoN(i);
            if (nodoEncontrado.getDato().getEstado().getCodigo()==1){
                resultado++;
            }
        }
        
        if (resultado+1==cantidadNodos){
            JsfUtil.addSuccessMessage("Usted es posicionad@ como ganador@");
            return true;
        }
        return false;
    }
}
