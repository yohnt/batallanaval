/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.modelo;

import java.io.Serializable;

/**
 *
 * @author tec_yohnt
 */
public class Usuario implements Serializable{
    private String correo; 
    private String contrasena; 
    private Rol tipoRol; 

    public Usuario(String correo, String contrasena, Rol tipoRol) {
        this.correo = correo;
        this.contrasena = contrasena;
        this.tipoRol = tipoRol;
    }

    
   

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Rol getTipoRol() {
        return tipoRol;
    }

    public void setTipoRol(Rol tipoRol) {
        this.tipoRol = tipoRol;
    }

    
    
     }
