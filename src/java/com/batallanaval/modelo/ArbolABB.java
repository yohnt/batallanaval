/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.modelo;

import com.batallanaval.excepcion.BarcoExepcion;
import com.batallanaval.validador.BarcoValidador;
import java.io.Serializable;

/**
 *
 * @author tec_yohnt
 */
public class ArbolABB implements Serializable{
  private NodoABB raiz;
  private int cantidadNodos;

    public ArbolABB() {
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public NodoABB getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoABB raiz) {
        this.raiz = raiz;
    }   
    
    
    
    
     public void adicionarNodo(TipoDeBarco dato) throws BarcoExepcion {
      
         BarcoValidador.validarDatos(dato);
      
        NodoABB nuevo = new NodoABB(dato);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            adicionarNodo(nuevo, raiz);
           cantidadNodos++; 
        }
        
    }

    private void adicionarNodo(NodoABB nuevo, NodoABB pivote)
            throws BarcoExepcion {
        
        String suplenNuevo= String.valueOf(nuevo.getDato().getNumeroCasillas());
        String suplenPivote= String.valueOf(pivote.getDato().getNumeroCasillas());
        if (suplenNuevo.compareTo(suplenPivote) == 0) {
            throw new BarcoExepcion("Ya existe un barco con el mismo numero de casillas "
                    + nuevo.getDato().getNumeroCasillas());
        } else if (suplenNuevo.compareTo(suplenPivote) < 0) {
            //Va por la izq
            if (pivote.getIzquierda() == null) {
                pivote.setIzquierda(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getIzquierda());
            }
        } else {
            //Va por la derecha
            if (pivote.getDerecha() == null) {
                pivote.setDerecha(nuevo);
            } else {
                adicionarNodo(nuevo, pivote.getDerecha());
            }
        }
    }
    public int sumarInOrdenRecursivo(NodoABB reco) {
        int suma = 0;
        if (reco != null) {
            suma += sumarInOrdenRecursivo(reco.getIzquierda());
            suma += reco.getDato().getCantidadJuego();
            suma += sumarInOrdenRecursivo(reco.getDerecha());
        }
        return suma;
    }
}
