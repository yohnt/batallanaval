/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tec_yohnt
 */
public class NodoN implements Serializable{
    private BarcoPocicionado dato;
    private List<NodoN> hijos;
    

    public NodoN(BarcoPocicionado dato) {
        this.dato = dato;
        hijos= new ArrayList<>();
        
    }

    public NodoN(List<NodoN> hijos) {
        this.hijos = hijos;
    }
    

    public BarcoPocicionado getDato() {
        return dato;
    }

    public void setDato(BarcoPocicionado dato) {
        this.dato = dato;
    }

    public List<NodoN> getHijos() {
        return hijos;
    }

    public void setHijos(List<NodoN> hijos) {
        this.hijos = hijos;
    }

    
    
}
