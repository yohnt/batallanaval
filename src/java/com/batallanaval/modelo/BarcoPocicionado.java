/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tec_yohnt
 */
public class BarcoPocicionado implements Serializable{
   private TipoDeBarco tipoDeBarco;
   private List<Coordenada> coordenadas;
   private byte numeroImpactos;
   private Estado estado;
   private int identificador;

    public BarcoPocicionado(TipoDeBarco tipoDeBarco, byte numeroImpactos, Estado estado) {
        this.tipoDeBarco = tipoDeBarco;
        this.numeroImpactos = numeroImpactos;
        this.estado = estado;
        coordenadas =new ArrayList<>();
    }

   
    public TipoDeBarco getTipoDeBarco() {
        return tipoDeBarco;
    }

    public void setTipoDeBarco(TipoDeBarco tipoDeBarco) {
        this.tipoDeBarco = tipoDeBarco;
    }

    public List<Coordenada> getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(List<Coordenada> coordenadas) {
        this.coordenadas = coordenadas;
    }

    public byte getNumeroImpactos() {
        return numeroImpactos;
    }

    public void setNumeroImpactos(byte numeroImpactos) {
        this.numeroImpactos = numeroImpactos;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    @Override
    public String toString() {
        return  this.getIdentificador()+" "+tipoDeBarco.getNombre() +" " + tipoDeBarco.getCantidadJuego();
    }

    
   
         
}