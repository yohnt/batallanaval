/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.modelo;

import java.io.Serializable;

/**
 *
 * @author tec_yohnt
 */
public class Coordenada implements Serializable{
    private byte columna; 
    private byte fila;
    private boolean estado;

    public Coordenada( byte fila, byte columna) {
        this.columna = columna;
        this.fila = fila;
           estado=true;
    }

    public byte getColumna() {
        return columna;
    }

    public void setColumna(byte columna) {
        this.columna = columna;
    }

    public byte getFila() {
        return fila;
    }

    public void setFila(byte fila) {
        this.fila = fila;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    
}
