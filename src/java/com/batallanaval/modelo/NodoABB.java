/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.batallanaval.modelo;

import java.io.Serializable;

/**
 *
 * @author tec_yohnt
 */
public class NodoABB implements Serializable{
    private TipoDeBarco dato;
    private NodoABB derecha;
    private NodoABB izquierda; 

    public NodoABB(TipoDeBarco dato) {
        this.dato = dato;
    }

    public TipoDeBarco getDato() {
        return dato;
    }

    public void setDato(TipoDeBarco dato) {
        this.dato = dato;
    }

    public NodoABB getDerecha() {
        return derecha;
    }

    public void setDerecha(NodoABB derecha) {
        this.derecha = derecha;
    }

    public NodoABB getIzquierda() {
        return izquierda;
    }

    public void setIzquierda(NodoABB izquierda) {
        this.izquierda = izquierda;
    }
    
}
